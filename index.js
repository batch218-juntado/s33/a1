/*btw it is not required to use async and await for this activity, make sure nagagawa lang po ang required feature with the right output po*/
/*https://jsonplaceholder.typicode.com/todos*/

const link = 'https://jsonplaceholder.typicode.com/todos'

/*
	3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
	4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
*/
fetch(`${link}`)
.then(response => response.json())
.then(array => console.log(array.map(item => item.title)));


/*
	5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
	6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
*/
fetch(`${link}/1`)
.then(response => response.json())
.then(item => console.log(`This item "${item.title}" on the list has a status of ${item.completed}`))


/*
7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
*/

fetch(`${link}`, {
	method: 'POST',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
    "title": "Created To Do List Item",
    "completed": false,
    "userId": 1,
    "id": 201
	})
})
.then(response => response.json())
.then(item => console.log(item))

/*
	8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
	9. Update a to do list item by changing the data structure to contain the following properties:
		- Title
		- Description
		- Status
		- Date Completed
		- User ID
*/
fetch(`${link}/1`, {
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
	"dateCompleted" : "Pending",
    "title": "Updated To Do List Item",
    "description" : "To update my to do list with different data structures.",
    "id":1,
    "status": "Pending",
    "userId": 1
	})
})
.then(response => response.json())
.then(item => console.log(item))


/*
	10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
	11. Update a to do list item by changing the status to complete and add a date when the status was changed.
*/
fetch(`${link}/1`, {
	method: 'PATCH',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
	"dateCompleted": "07/09/21",
    "status": "Complete",
	})
})
.then(response => response.json())
.then(item => console.log(item))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch(`${link}/3`, {
	method: 'DELETE',
})
.then(response => response.json())
.then(item => console.log(item))


